package lab4.partc;

public final class PayCheck {
    private double grossPay;
    private double fica;
    private double state;
    private double local;
    private double medicare;
    private double socialSecurity;

    PayCheck(double grossPay, double fica, double state, double local, double medicare, double socialSecurity) {
        this.grossPay = grossPay;
        this.fica = fica;
        this.state = state;
        this.local = local;
        this.medicare = medicare;
        this.socialSecurity = socialSecurity;
    }

    public void print() {
        System.out.println(toString());
    }

    public double getNetPay() {
        return grossPay - (fica + state + local + medicare + socialSecurity);
    }

    @Override
    public String toString(){
        return ("******** PAYCHECK ******\n"+
                " FICA : " + fica +
                "\n State: " + state +
                "\n Local: " + local +
                "\n Medicare: " + medicare +
                "\n SocialSecurity: " + socialSecurity+
                "\n GrossPay: "+grossPay+
                "\n NetPay: "+getNetPay()
        );
    }
}
