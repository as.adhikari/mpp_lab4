package lab4.partc;

public abstract class Employee {
    private String empID;

    Employee(String empId){
        this.empID = empId;
    }

    public void print(int year, int month){
        PayCheck payCheck = calcCompensation(year, month);
        System.out.println("EmployID: "+empID);
        System.out.println(payCheck);

    }

    public PayCheck calcCompensation(int year, int month){

        double grossPay = calcGrossPay(year,month);
        double fica = grossPay * 0.23;
        double state = grossPay * 0.05;
        double local = grossPay * 0.01;
        double medicare = grossPay * 0.03;
        double socialSecurity = grossPay * 0.075;

        return new PayCheck(grossPay,fica,state,local,medicare,socialSecurity);
    }

    public abstract double calcGrossPay(int year, int month);

}
