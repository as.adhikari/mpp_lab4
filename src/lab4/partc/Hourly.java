package lab4.partc;

public class Hourly extends Employee{
    private double hourlyWage;
    private double hoursPerWeek;
    private final static int totalWeeks = 4;

    Hourly(String id, double hourlyWage, double hoursPerWeek){
        super(id);
        this.hourlyWage = hourlyWage;
        this.hoursPerWeek = hoursPerWeek;

    }
    @Override
    public double calcGrossPay(int year, int month) {
        return (hourlyWage*hoursPerWeek*totalWeeks);
    }
}
