package lab4.partc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args){
        int year = 2018; int month = 8;
        Employee salaried = new Salaried("EMP1",5600);
        System.out.println("========== ================= ========");
        System.out.println("--------- Salaried Employee --------");
        System.out.println("========== ================= ========");
        salaried.print(year,month);

        Employee hourly = new Hourly("EMP2", 40, 40);
        System.out.println("========== ================= ========");
        System.out.println("--------- Hourly Employee ----------");
        System.out.println("========== ================= ========");
        hourly.print(year, month);

        LocalDate orderDate = LocalDate.parse("2018-08-07");
        Order order1 = new Order("ORD1", orderDate, 3000);
        Order order2 = new Order("ORD2", orderDate, 1200);
        List<Order> orders = new ArrayList<>();
        orders.add(order1);
        orders.add(order2);
        Employee commissioned = new Commissioned("EMP3",0.05,2000, orders);

        System.out.println("========== ================= ========");
        System.out.println("--------- Commissioned Employee---------");
        System.out.println("========== ================= ========");
        commissioned.print(year,month);
    }
}
