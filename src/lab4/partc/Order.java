package lab4.partc;

import java.time.LocalDate;

public class Order {
    private String orderNum;
    private LocalDate orderDate;
    private double orderAmount;

    Order(String orderNum, LocalDate orderDate, double orderAmount){
        this.orderNum = orderNum;
        this.orderDate = orderDate;
        this.orderAmount = orderAmount;
    }

    public LocalDate getOrderDate(){
        return orderDate;
    }

    public double getOrderAmount(){
        return orderAmount;
    }
}
