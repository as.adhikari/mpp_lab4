package lab4.partc;

public class Salaried extends Employee {
    private double salary;

    Salaried(String id, double salary){
        super(id);
        this.salary = salary;
    }


    @Override
    public double calcGrossPay(int year, int month) {
        return salary;
    }
}
