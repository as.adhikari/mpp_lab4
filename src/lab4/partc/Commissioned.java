package lab4.partc;

import java.util.ArrayList;
import java.util.List;

public class Commissioned extends Employee {
    private double commission;
    private double baseSalary;
    private List<Order> orders;

    Commissioned(String id, double commission, double baseSalary){
        super(id);
        this.commission = commission;
        this.baseSalary = baseSalary;
        this.orders = new ArrayList<>();
    }

    Commissioned(String id, double commission, double baseSalary, List<Order> orders){
        super(id);
        this.commission = commission;
        this.baseSalary = baseSalary;
        this.orders = orders;
    }

    public void addOrder(Order order){
        orders.add(order);
    }

    @Override
    public double calcGrossPay(int year, int month) {
        return baseSalary + (calcCommissionAmount(year,month) * commission);
    }

    public double calcCommissionAmount(int year, int month){
        double totalOrderAmount = 0.0;
        int lastMonthYear = year; int lastMonth = month-1;
        if(month == 1){
            lastMonthYear = year-1;
            lastMonth = 12;
        }
        
        for(Order order: orders){
            if((order.getOrderDate().getYear() == lastMonthYear) &&
                    (order.getOrderDate().getMonthValue() == lastMonth))
            {
                totalOrderAmount += order.getOrderAmount();
            }
        }

        return totalOrderAmount;
    }
}
